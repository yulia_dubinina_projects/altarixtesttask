package testtask;

public interface Farm {

    public void giveBirth(long parentCowId, long childCowId, String childNickName) throws FarmException;

    public void endLifeSpan(long cowId) throws FarmException;

    public void printFarmData();
}


