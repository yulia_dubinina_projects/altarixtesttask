package testtask;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Enter the first cow nick name:");
        Scanner scanner = new Scanner(System.in);
        String line = "";
        boolean first = true;
        while (line.isEmpty()) {
            if (!first) {
                System.out.println("Name should not be empty. Try again");
            } else {
                first = false;
            }
            line = scanner.nextLine().trim();
        }
        Farm farm = new FarmTreeList(new Cow(0, line));

        first = true;

        while (true) {
            int command;
            if (first) {
                System.out.println("------------------");
                System.out.println("Current farm:");
                System.out.println("------------------");
                farm.printFarmData();
                System.out.println("------------------");
                System.out.println("Enter command number and args divided by space (0 - exit, 1 - giveBirth, 2 - endLifeSpan):");
                first = false;
            } else {
                System.out.println("Wrong command or args format. Try again");
            }
            line = scanner.nextLine().trim();
            int c = 0;
            while (c < line.length() && !Character.isWhitespace(line.charAt(c))) {
                c++;
            }
            try {
                command = Integer.parseInt(line.substring(0, c));

            } catch (Throwable e) {
                continue;
            }
            if (command == 0) {
                System.exit(0);
            } else if (command == 1 || command == 2) {
                c++;
                while (c < line.length() && Character.isWhitespace(line.charAt(c))) {
                    c++;
                }
                if (c == line.length()) {
                    continue;
                }
                int b = c;
                while (c < line.length() && !Character.isWhitespace(line.charAt(c))) {
                    c++;
                }
                long arg1;
                try {
                    arg1 = Long.parseLong(line.substring(b, c));
                } catch (Throwable e) {
                    continue;
                }
                if (command == 1) {
                    while (c < line.length() && Character.isWhitespace(line.charAt(c))) {
                        c++;
                    }
                    if (c == line.length()) {
                        continue;
                    }
                    b = c;
                    while (c < line.length() && !Character.isWhitespace(line.charAt(c))) {
                        c++;
                    }
                    long arg2;
                    try {
                        arg2 = Long.parseLong(line.substring(b, c));
                    } catch (Throwable e) {
                        continue;
                    }
                    String nickName = line.substring(c).trim();
                    if (nickName.isEmpty()) {
                        continue;
                    }

                    first = true;
                    try {
                        farm.giveBirth(arg1, arg2, nickName);
                    } catch (FarmException e) {
                        System.out.println("Error performing command: " + e.getMessage());
                        continue;
                    }
                } else {
                    first = true;
                    try {
                        farm.endLifeSpan(arg1);
                    } catch (FarmException e) {
                        System.out.println("Error performing command: " + e.getMessage());
                        continue;
                    }
                }
            }
        }

    }
}
