package testtask;

public class Cow {

    private long cowId;
    private String nickName;

    public Cow(long cowId, String nickName) {
        this.cowId = cowId;
        this.nickName = nickName;
    }

    public long getCowId() {
        return cowId;
    }

    public void setCowId(long cowId) {
        this.cowId = cowId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Override
    public String toString() {
        return "Cow id=" + cowId + " nickName=[" + nickName + "]";
    }

    @Override
    public int hashCode() {
        int result = (int) (cowId ^ (cowId >>> 32));
        result = 31 * result + nickName.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Cow)) {
            return false;
        }
        Cow c = (Cow) obj;
        return c.cowId == this.cowId && c.nickName.equals(this.nickName);
    }
}
