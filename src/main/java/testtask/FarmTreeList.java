package testtask;

public class FarmTreeList implements Farm {

    private class NodeEntry {
        Cow cow;
        NodeEntry parent;
        NodeEntry prevSibling;
        NodeEntry nextSibling;
        NodeEntry firstChild;

        public NodeEntry(Cow cow, NodeEntry parent, NodeEntry prevSibling, NodeEntry nextSibling, NodeEntry firstChild) {
            this.cow = cow;
            this.parent = parent;
            this.prevSibling = prevSibling;
            this.nextSibling = nextSibling;
            this.firstChild = firstChild;
        }
    }

    private NodeEntry rootList;

    public FarmTreeList(Cow theFirstCow) {
        rootList = new NodeEntry(theFirstCow, null, null, null, null);
    }

    private NodeEntry iterateTreeForBirth(NodeEntry node, long parentCowId, long childCowId) throws FarmException {
        NodeEntry ne = node;
        NodeEntry result = null;
        while (ne != null) {
            if (ne.cow.getCowId() == parentCowId) {
                result = ne;
            }
            if (ne.cow.getCowId() == childCowId) {
                throw new FarmException("A cow with child id=" + childCowId + " already exists");
            }
            NodeEntry sub = iterateTreeForBirth(ne.firstChild, parentCowId, childCowId);
            if (sub != null) {
                result = sub;
            }
            ne = ne.nextSibling;
        }
        return result;
    }

    private NodeEntry iterateTreeForRemoval(NodeEntry node, long cowId) throws FarmException {
        NodeEntry ne = node;
        while (ne != null) {
            if (ne.cow.getCowId() == cowId) {
                return ne;
            }
            NodeEntry sub = iterateTreeForRemoval(ne.firstChild, cowId);
            if (sub != null) {
                return sub;
            }
            ne = ne.nextSibling;
        }
        return null;
    }

    public synchronized void giveBirth(long parentCowId, long childCowId, String childNickName) throws FarmException {
        NodeEntry parentNode = iterateTreeForBirth(rootList, parentCowId, childCowId);
        if (parentNode == null) {
            throw new FarmException("A cow with parent id=" + parentCowId + " does not exist");
        }
        Cow child = new Cow(childCowId, childNickName);
        if (parentNode.firstChild == null) {
            NodeEntry childNode = new NodeEntry(child, parentNode, null, null, null);
            parentNode.firstChild = childNode;
        } else {
            NodeEntry prevChild = parentNode.firstChild;
            while (prevChild.nextSibling != null) {
                prevChild = prevChild.nextSibling;
            }
            NodeEntry childNode = new NodeEntry(child, parentNode, prevChild, null, null);
            prevChild.nextSibling = childNode;
        }
    }

    public synchronized void endLifeSpan(long cowId) throws FarmException {
        NodeEntry ne = iterateTreeForRemoval(rootList, cowId);
        if (ne == null) {
            throw new FarmException("A cow with id=" + cowId + " does not exist");
        }
        if (ne == rootList && ne.nextSibling == null && ne.firstChild == null) {
            throw new FarmException("This is the last cow. Farm cannot exist without cows");
        }

        if (ne.firstChild != null) {
            NodeEntry lastChildSibling = ne.firstChild;
            lastChildSibling.parent = ne.parent;
            while (lastChildSibling.nextSibling != null) {
                lastChildSibling = lastChildSibling.nextSibling;
                lastChildSibling.parent = ne.parent;
            }
            lastChildSibling.nextSibling = ne.nextSibling;
            ne.firstChild.prevSibling = ne.prevSibling;
            if (ne.prevSibling == null) {
                if (ne.parent == null) {
                    rootList = ne.firstChild;
                } else {
                    ne.parent.firstChild = ne.firstChild;
                }
            } else {
                ne.prevSibling.nextSibling = ne.firstChild;
            }
        } else {
            if (ne.prevSibling == null) {
                if (ne.parent == null) {
                    rootList = ne.nextSibling;
                } else {
                    ne.parent.firstChild = ne.nextSibling;
                }
            } else {
                ne.prevSibling.nextSibling = ne.nextSibling;
            }
        }
    }

    private void iterateTreeForPrint(NodeEntry node, String ident, boolean root) {
        NodeEntry ne = node;
        while (ne != null) {
            System.out.println(ident + ne.cow.toString());
            iterateTreeForPrint(ne.firstChild, root ? "|-" : "  " + ident, false);
            ne = ne.nextSibling;
        }
    }

    public synchronized void printFarmData() {
        iterateTreeForPrint(rootList, "", true);
    }
}