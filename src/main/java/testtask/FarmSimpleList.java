package testtask;

public class FarmSimpleList implements Farm {

    private class ListEntry {
        Cow value;
        ListEntry next;

        public ListEntry(Cow value, ListEntry next) {
            this.value = value;
            this.next = next;
        }
    }

    private ListEntry cowList;

    public FarmSimpleList(Cow theFirstCow) {
        cowList = new ListEntry(theFirstCow, null);
    }

    public synchronized void giveBirth(long parentCowId, long childCowId, String childNickName) throws FarmException {
        ListEntry le = cowList;
        Cow parent = null;
        while (le != null) {
            if (le.value.getCowId() == childCowId) {
                throw new FarmException("A cow with child id=" + childCowId + " already exists");
            }
            if (le.value.getCowId() == parentCowId) {
                parent = le.value;
            }
            le = le.next;
        }
        if (parent == null) {
            throw new FarmException("A cow with parent id=" + parentCowId + " does not exist");
        }
        Cow child = new Cow(childCowId, childNickName);
        le = new ListEntry(child, cowList);
        cowList = le;
    }

    public synchronized void endLifeSpan(long cowId) throws FarmException {
        ListEntry le = cowList;
        ListEntry prev = null;
        while (le != null) {
            if (le.value.getCowId() == cowId) {
                if (prev == null) {
                    if (cowList.next == null) {
                        throw new FarmException("This is the last cow. Farm cannot exist without cows");
                    }
                    cowList = cowList.next;
                } else {
                    prev.next = le.next;
                }
                return;
            }
            prev = le;
            le = le.next;
        }
        throw new FarmException("A cow with id=" + cowId + " does not exist");
    }

    public synchronized void printFarmData() {
        ListEntry le = cowList;
        while (le != null) {
            System.out.println(le.value.toString());
            le = le.next;
        }
    }
}
