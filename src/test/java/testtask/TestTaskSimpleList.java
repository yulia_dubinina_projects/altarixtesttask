package testtask;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class TestTaskSimpleList {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }


    private void assertCowSet(Set<Cow> cows, String output) {
        restoreStreams();
        System.out.println("Got to check:\n"+output);
        setUpStreams();
        BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(output.getBytes())));
        String line;
        int counter = 0;
        try {
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty()) {
                    continue;
                }
                String id = line.substring("Cow id=".length(), line.indexOf(" nickName=["));
                String name = line.substring(line.indexOf(" nickName=[") + " nickName=[".length(), line.length() - 1);
                Cow cow = new Cow(Long.parseLong(id), name);
                if (!cows.contains(cow)) {
                    Assert.fail("Non existent cow in output");
                }
                counter++;
            }
            br.close();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
        Assert.assertEquals(cows.size(), counter);
    }

    @Test
    public void test() {
        Cow greatMom = new Cow(1, "Great mom");
        FarmSimpleList farm = new FarmSimpleList(greatMom);
        Set<Cow> farmSet = new HashSet<>();
        farmSet.add(greatMom);
        farm.printFarmData();
        assertCowSet(farmSet, outContent.toString());
        outContent.reset();

        Cow burenka = new Cow(2, "Burenka");
        try {
            farm.giveBirth(greatMom.getCowId(), burenka.getCowId(), burenka.getNickName());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farmSet.add(burenka);
        farm.printFarmData();
        assertCowSet(farmSet, outContent.toString());
        outContent.reset();

        Cow murka = new Cow(3, "Murka");
        try {
            farm.giveBirth(greatMom.getCowId(), murka.getCowId(), murka.getNickName());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farmSet.add(murka);
        farm.printFarmData();
        assertCowSet(farmSet, outContent.toString());
        outContent.reset();

        Cow milka = new Cow(4, "Milka");
        try {
            farm.giveBirth(murka.getCowId(), milka.getCowId(), milka.getNickName());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farmSet.add(milka);
        farm.printFarmData();
        assertCowSet(farmSet, outContent.toString());
        outContent.reset();

        try {
            farm.giveBirth(-1, 100, "some cow");
        } catch (FarmException e) {
            Assert.assertEquals("A cow with parent id=-1 does not exist", e.getMessage());
        }

        try {
            farm.giveBirth(milka.getCowId(), greatMom.getCowId(), "some cow");
        } catch (FarmException e) {
            Assert.assertEquals("A cow with child id=" + greatMom.getCowId() + " already exists", e.getMessage());
        }

        try {
            farm.endLifeSpan(-1);
        } catch (FarmException e) {
            Assert.assertEquals("A cow with id=-1 does not exist", e.getMessage());
        }

        try {
            farm.endLifeSpan(burenka.getCowId());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farmSet.remove(burenka);
        farm.printFarmData();
        assertCowSet(farmSet, outContent.toString());
        outContent.reset();

        try {
            farm.endLifeSpan(greatMom.getCowId());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farmSet.remove(greatMom);
        farm.printFarmData();
        assertCowSet(farmSet, outContent.toString());
        outContent.reset();

        try {
            farm.endLifeSpan(murka.getCowId());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farmSet.remove(murka);
        farm.printFarmData();
        assertCowSet(farmSet, outContent.toString());
        outContent.reset();

        try {
            farm.endLifeSpan(milka.getCowId());
        } catch (FarmException e) {
            Assert.assertEquals("This is the last cow. Farm cannot exist without cows", e.getMessage());
        }

    }

}
