package testtask;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

public class TestTaskTreeList {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }


    private void assertOutput(String[] expected, String output) {
        restoreStreams();
        System.out.println("Got to check:\n" + output);
        setUpStreams();
        BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(output.getBytes())));
        String line;
        int counter = 0;
        try {
            while ((line = br.readLine()) != null) {
                if (line.trim().isEmpty()) {
                    continue;
                }
                line = line.substring(0, line.lastIndexOf("]") + 1);
                Assert.assertEquals(expected[counter], line);
                counter++;
            }
            br.close();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
        Assert.assertEquals(expected.length, counter);
    }

    @Test
    public void test0() {
        Cow greatMom = new Cow(1, "Great mom");
        FarmTreeList farm = new FarmTreeList(greatMom);
        farm.printFarmData();
        assertOutput(new String[]{greatMom.toString()}, outContent.toString());
        outContent.reset();

        Cow burenka = new Cow(2, "Burenka");
        try {
            farm.giveBirth(greatMom.getCowId(), burenka.getCowId(), burenka.getNickName());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farm.printFarmData();
        assertOutput(new String[]{
                greatMom.toString(),
                "|-" + burenka.toString()
        }, outContent.toString());
        outContent.reset();

        Cow murka = new Cow(3, "Murka");
        try {
            farm.giveBirth(greatMom.getCowId(), murka.getCowId(), murka.getNickName());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farm.printFarmData();
        assertOutput(new String[]{
                greatMom.toString(),
                "|-" + burenka.toString(),
                "|-" + murka.toString()
        }, outContent.toString());
        outContent.reset();

        Cow milka = new Cow(4, "Milka");
        try {
            farm.giveBirth(murka.getCowId(), milka.getCowId(), milka.getNickName());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farm.printFarmData();
        assertOutput(new String[]{
                greatMom.toString(),
                "|-" + burenka.toString(),
                "|-" + murka.toString(),
                "  |-" + milka.toString()
        }, outContent.toString());
        outContent.reset();

        try {
            farm.giveBirth(-1, 100, "some cow");
        } catch (FarmException e) {
            Assert.assertEquals("A cow with parent id=-1 does not exist", e.getMessage());
        }

        try {
            farm.giveBirth(milka.getCowId(), greatMom.getCowId(), "some cow");
        } catch (FarmException e) {
            Assert.assertEquals("A cow with child id=" + greatMom.getCowId() + " already exists", e.getMessage());
        }

        try {
            farm.endLifeSpan(-1);
        } catch (FarmException e) {
            Assert.assertEquals("A cow with id=-1 does not exist", e.getMessage());
        }

        try {
            farm.endLifeSpan(burenka.getCowId());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farm.printFarmData();
        assertOutput(new String[]{
                greatMom.toString(),
                "|-" + murka.toString(),
                "  |-" + milka.toString()
        }, outContent.toString());
        outContent.reset();

        try {
            farm.endLifeSpan(greatMom.getCowId());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farm.printFarmData();
        assertOutput(new String[]{
                murka.toString(),
                "|-" + milka.toString()
        }, outContent.toString());
        outContent.reset();

        try {
            farm.endLifeSpan(murka.getCowId());
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }
        farm.printFarmData();
        assertOutput(new String[]{
                milka.toString()
        }, outContent.toString());
        outContent.reset();

        try {
            farm.endLifeSpan(milka.getCowId());
        } catch (FarmException e) {
            Assert.assertEquals("This is the last cow. Farm cannot exist without cows", e.getMessage());
        }

    }

    @Test
    public void test1() {

        Cow greatMom = new Cow(0, "Great mom");
        FarmTreeList farm = new FarmTreeList(greatMom);
        try {
            farm.giveBirth(0, 1, "First level child 1");
            farm.giveBirth(0, 2, "First level child 2");
            farm.giveBirth(0, 3, "First level child 3");
            farm.giveBirth(0, 4, "First level child 4");

            farm.printFarmData();
            assertOutput(new String[]{
                    greatMom.toString(),
                    "|-Cow id=1 nickName=[First level child 1]",
                    "|-Cow id=2 nickName=[First level child 2]",
                    "|-Cow id=3 nickName=[First level child 3]",
                    "|-Cow id=4 nickName=[First level child 4]"
            }, outContent.toString());
            outContent.reset();

            farm.endLifeSpan(0);

            farm.printFarmData();
            assertOutput(new String[]{
                    "Cow id=1 nickName=[First level child 1]",
                    "Cow id=2 nickName=[First level child 2]",
                    "Cow id=3 nickName=[First level child 3]",
                    "Cow id=4 nickName=[First level child 4]"
            }, outContent.toString());
            outContent.reset();

            farm.giveBirth(1, 5, "Second level child 1-1");
            farm.giveBirth(1, 6, "Second level child 1-2");
            farm.giveBirth(1, 7, "Second level child 1-3");
            farm.giveBirth(1, 8, "Second level child 1-4");

            farm.printFarmData();
            assertOutput(new String[]{
                    "Cow id=1 nickName=[First level child 1]",
                    "|-Cow id=5 nickName=[Second level child 1-1]",
                    "|-Cow id=6 nickName=[Second level child 1-2]",
                    "|-Cow id=7 nickName=[Second level child 1-3]",
                    "|-Cow id=8 nickName=[Second level child 1-4]",
                    "Cow id=2 nickName=[First level child 2]",
                    "Cow id=3 nickName=[First level child 3]",
                    "Cow id=4 nickName=[First level child 4]"
            }, outContent.toString());
            outContent.reset();

            farm.giveBirth(5, 9, "Third level child 1-1-1");
            farm.giveBirth(5, 10, "Third level child 1-1-2");
            farm.giveBirth(5, 11, "Third level child 1-1-3");
            farm.giveBirth(5, 12, "Third level child 1-1-4");

            farm.printFarmData();
            assertOutput(new String[]{
                    "Cow id=1 nickName=[First level child 1]",
                    "|-Cow id=5 nickName=[Second level child 1-1]",
                    "  |-Cow id=9 nickName=[Third level child 1-1-1]",
                    "  |-Cow id=10 nickName=[Third level child 1-1-2]",
                    "  |-Cow id=11 nickName=[Third level child 1-1-3]",
                    "  |-Cow id=12 nickName=[Third level child 1-1-4]",
                    "|-Cow id=6 nickName=[Second level child 1-2]",
                    "|-Cow id=7 nickName=[Second level child 1-3]",
                    "|-Cow id=8 nickName=[Second level child 1-4]",
                    "Cow id=2 nickName=[First level child 2]",
                    "Cow id=3 nickName=[First level child 3]",
                    "Cow id=4 nickName=[First level child 4]"
            }, outContent.toString());
            outContent.reset();


            farm.endLifeSpan(7);
            farm.endLifeSpan(10);
            farm.endLifeSpan(9);

            farm.printFarmData();
            assertOutput(new String[]{
                    "Cow id=1 nickName=[First level child 1]",
                    "|-Cow id=5 nickName=[Second level child 1-1]",
                    "  |-Cow id=11 nickName=[Third level child 1-1-3]",
                    "  |-Cow id=12 nickName=[Third level child 1-1-4]",
                    "|-Cow id=6 nickName=[Second level child 1-2]",
                    "|-Cow id=8 nickName=[Second level child 1-4]",
                    "Cow id=2 nickName=[First level child 2]",
                    "Cow id=3 nickName=[First level child 3]",
                    "Cow id=4 nickName=[First level child 4]"
            }, outContent.toString());
            outContent.reset();

            farm.giveBirth(6, 13, "Third level child 1-2-1");
            farm.giveBirth(6, 14, "Third level child 1-2-2");
            farm.giveBirth(6, 15, "Third level child 1-2-3");
            farm.giveBirth(6, 16, "Third level child 1-2-4");

            farm.printFarmData();
            assertOutput(new String[]{
                    "Cow id=1 nickName=[First level child 1]",
                    "|-Cow id=5 nickName=[Second level child 1-1]",
                    "  |-Cow id=11 nickName=[Third level child 1-1-3]",
                    "  |-Cow id=12 nickName=[Third level child 1-1-4]",
                    "|-Cow id=6 nickName=[Second level child 1-2]",
                    "  |-Cow id=13 nickName=[Third level child 1-2-1]",
                    "  |-Cow id=14 nickName=[Third level child 1-2-2]",
                    "  |-Cow id=15 nickName=[Third level child 1-2-3]",
                    "  |-Cow id=16 nickName=[Third level child 1-2-4]",
                    "|-Cow id=8 nickName=[Second level child 1-4]",
                    "Cow id=2 nickName=[First level child 2]",
                    "Cow id=3 nickName=[First level child 3]",
                    "Cow id=4 nickName=[First level child 4]"
            }, outContent.toString());
            outContent.reset();

            farm.endLifeSpan(6);

            farm.printFarmData();
            assertOutput(new String[]{
                    "Cow id=1 nickName=[First level child 1]",
                    "|-Cow id=5 nickName=[Second level child 1-1]",
                    "  |-Cow id=11 nickName=[Third level child 1-1-3]",
                    "  |-Cow id=12 nickName=[Third level child 1-1-4]",
                    "|-Cow id=13 nickName=[Third level child 1-2-1]",
                    "|-Cow id=14 nickName=[Third level child 1-2-2]",
                    "|-Cow id=15 nickName=[Third level child 1-2-3]",
                    "|-Cow id=16 nickName=[Third level child 1-2-4]",
                    "|-Cow id=8 nickName=[Second level child 1-4]",
                    "Cow id=2 nickName=[First level child 2]",
                    "Cow id=3 nickName=[First level child 3]",
                    "Cow id=4 nickName=[First level child 4]"
            }, outContent.toString());
            outContent.reset();

            farm.endLifeSpan(1);

            farm.printFarmData();
            assertOutput(new String[]{
                    "Cow id=5 nickName=[Second level child 1-1]",
                    "|-Cow id=11 nickName=[Third level child 1-1-3]",
                    "|-Cow id=12 nickName=[Third level child 1-1-4]",
                    "Cow id=13 nickName=[Third level child 1-2-1]",
                    "Cow id=14 nickName=[Third level child 1-2-2]",
                    "Cow id=15 nickName=[Third level child 1-2-3]",
                    "Cow id=16 nickName=[Third level child 1-2-4]",
                    "Cow id=8 nickName=[Second level child 1-4]",
                    "Cow id=2 nickName=[First level child 2]",
                    "Cow id=3 nickName=[First level child 3]",
                    "Cow id=4 nickName=[First level child 4]"
            }, outContent.toString());
            outContent.reset();
        } catch (FarmException e) {
            Assert.fail(e.getMessage());
        }

    }
}
