#Coding Assignment

**Question 1 (33%)**

A dairy farmer wishes to manage the breeding of the cows in
his farm. For this reason, he defines the Cow entity (cowId,
nickName). Cows can give birth to calves by insemination and
end their life span.  

Your mission is to create a data structure to support the dairy
farm and the following operations on it (assume you start with
a single cow that is always alive, and that all calves are born
female):

1. GiveBirth (parentCowId, childCowId, childNickName) – adds a new female calf to the farm.
2. EndLifeSpan (cowId) – removes the cow from the farm.
3. Print farm data – outputs entire farm to the standard output in a readable manner.

**Question 2 (67%)**

Repeat question 1 with the following limitation: you CAN NOT
use Java built in arrays/collections/lists/maps (or any data
structure which inherits or uses them). So, for instance using
the brackets “[“or “]” anywhere is forbidden. You must
implement your own data structure to support the same
operations.  

Good Luck!

#Commets:
* I asked if I can skip Question 1 and do Question 2 right away. I was told that I can
* I wrote FarmSimpleList and tests for it, becuase in task description it is nowhere said we want to see parent-child relationships.
* Then I was told to output a farm in a tree view. If cow dies all her children should be lifted to one level up. I wrote FarmTreeList and tests for it.
* Then I was aked to write simple text interface to play with my solution and test it. And I wrote Main.
